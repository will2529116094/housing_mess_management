/*
Navicat MySQL Data Transfer

Source Server         : first
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : housing_info_management

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-06-03 15:46:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for resident
-- ----------------------------
DROP TABLE IF EXISTS `resident`;
CREATE TABLE `resident` (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT '主码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '姓名',
  `gender` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '性别',
  `birthday` date DEFAULT '1995-10-01' COMMENT '出生日期',
  `telephone_number` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `identity_card` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '身份证号',
  `census_register` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '户籍所在地',
  `educational_level` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '受教育程度',
  `party_id` int(3) DEFAULT '0' COMMENT '所属党派id',
  `photo` varchar(50) DEFAULT NULL COMMENT '居民照片',
  `is_head_of_household` int(1) NOT NULL DEFAULT '1' COMMENT '是否是户主',
  `house_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属房间id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_index` (`id`),
  KEY `resident_ibfk_1` (`house_id`),
  KEY `party` (`party_id`),
  CONSTRAINT `resident_ibfk_1` FOREIGN KEY (`house_id`) REFERENCES `house` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `resident_ibfk_2` FOREIGN KEY (`party_id`) REFERENCES `party` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

