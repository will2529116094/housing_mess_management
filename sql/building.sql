/*
Navicat MySQL Data Transfer

Source Server         : first
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : housing_info_management

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-05-25 17:07:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for building
-- ----------------------------
DROP TABLE IF EXISTS `building`;
CREATE TABLE `building` (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主码（建成时间+楼号）',
  `building_number` int(3) NOT NULL COMMENT '楼宇号',
  `construction_time` date NOT NULL COMMENT '建筑时间',
  `property_management_company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '物业管理公司',
  `building_principal` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '负责人',
  `unit_count` int(3) NOT NULL DEFAULT '0' COMMENT '单元数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


