/*
Navicat MySQL Data Transfer

Source Server         : first
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : housing_info_management

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-05-25 17:08:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for house
-- ----------------------------
DROP TABLE IF EXISTS `house`;
CREATE TABLE `house` (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主码（单元id+房间号）',
  `house_number` int(5) NOT NULL COMMENT '房间号码',
  `house_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '户型',
  `house_area` int(5) NOT NULL DEFAULT '120' COMMENT '房间面积',
  `house_people_count` int(2) DEFAULT '0' COMMENT '房屋所住人数',
  `house_risk_level` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '住房危险性鉴定等级（A/B/C/D）',
  `house_use_status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '房屋使用现状（自用、租借、闲置）',
  `room_count` int(2) NOT NULL DEFAULT '5' COMMENT '房间数量',
  `sale_info` int(1) NOT NULL DEFAULT '0' COMMENT '是否出售(0或1)',
  `check_in_time` date DEFAULT '0000-00-00' COMMENT '入住时间',
  `unit_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属单元id',
  PRIMARY KEY (`id`),
  KEY `house_ibfk_1` (`unit_id`),
  CONSTRAINT `house_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

