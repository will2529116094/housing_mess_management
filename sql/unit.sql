/*
Navicat MySQL Data Transfer

Source Server         : first
Source Server Version : 80016
Source Host           : localhost:3306
Source Database       : housing_info_management

Target Server Type    : MYSQL
Target Server Version : 80016
File Encoding         : 65001

Date: 2020-05-25 17:07:48
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主码（楼宇号+单元号）',
  `unit_number` int(3) NOT NULL COMMENT '单元号',
  `layer_count` int(3) NOT NULL COMMENT '层数',
  `house_count` int(5) NOT NULL COMMENT '每层房间数量',
  `elevator_count` int(3) NOT NULL COMMENT '电梯数量',
  `building_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属楼宇id',
  PRIMARY KEY (`id`),
  KEY `unit_ibfk_1` (`building_id`),
  CONSTRAINT `unit_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `building` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


